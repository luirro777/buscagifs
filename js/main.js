const apiKey = 'TU_API_KEY'; // Reemplaza 'TU_API_KEY' por tu clave de API de Giphy

// Obtener referencia al botón de búsqueda
const searchButton = document.getElementById('searchButton');

// Agregar el evento de clic al botón de búsqueda
searchButton.addEventListener('click', () => {
    const input = document.getElementById('gifInput');
    const query = input.value;
  
    // Limpia los resultados anteriores
    const gifResults = document.getElementById('gifResults');
    gifResults.innerHTML = '';
  
    // Realiza la petición a la API de Giphy
    fetch(`https://api.giphy.com/v1/gifs/search?api_key=${apiKey}&q=${query}&limit=10`)
      .then(response => response.json())
      .then(data => {
        const gifs = data.data;
        gifs.forEach(gif => {
          // Crea un elemento de imagen y agrega el GIF
          const img = document.createElement('img');
          img.src = gif.images.fixed_height.url;
          img.alt = gif.title;
  
          // Crea una tarjeta para el GIF
          const gifCard = document.createElement('div');
          gifCard.classList.add('gifCard');
          gifCard.appendChild(img);
  
          // Agrega la tarjeta al contenedor de resultados
          gifResults.appendChild(gifCard);
        });
      })
      .catch(error => {
        console.error('Error al buscar gifs:', error);
      });
  });